'use strict';
const Generator = require('yeoman-generator');
const gitParser = require('git-url-parse');
const chalk = require('chalk');
const yosay = require('yosay');
const toml = require('@iarna/toml');
// @ts-ignore
const git = require('simple-git/promise');
const gitRemoteOriginUrl = require('git-remote-origin-url');
const slug = require('slugify');
const fs = require('fs');
const fse = require('fs-extra');
const templates = require('./repos.json');
const targetDir = 'target-dir';

module.exports = class extends Generator {
  async prompting () {
    // Have Yeoman greet the user.
    this.log(
      // @ts-ignore
      yosay(`Welcome to the extraordinary ${chalk.red('uplb-hci-lab-project-template')} generator! This is for a Gitlab project repo generator`)
    );

    this.log(
      'Please take note that the system assumes that this is already a git enabled project'
    );

    let gitRepo;
    try {
      gitRepo = await gitRemoteOriginUrl();
    } catch (error) {
      console.log('No git config found');
    }

    let readme = '';

    try {
      readme = fs.readFileSync('./README.md', 'utf8');
    } catch (error) {
      console.log('No README found');
    }

    const readmeArr = readme.split('\n\n');

    const prompts = [
      {
        type: 'input',
        name: 'projectName',
        message: 'What is the name of the project?',
        default: readmeArr[0].replace('# ', '') || this.appname
      },
      {
        type: 'input',
        name: 'projectDescription',
        message: 'What is the summary description of the project?',
        default: readmeArr[1]
      },
      {
        type: 'input',
        name: 'projectRepo',
        message: 'What is the GIT repository URL of the project?',
        default: gitRepo || 'git@gitlab.com:organization/repo.git'
      },
      {
        type: 'input',
        name: 'author',
        message: 'Who is the author of the project?',
        default: 'Generic Group'
      },
      {
        type: 'list',
        name: 'templateRepo',
        message: 'What project template will you clone?',
        choices: templates.map(item => item.name),
        default: 0
      }
    ];

    const props = await this.prompt(prompts);

    this.props = props;
  }

  async writing () {
    /**
     * @type {{ props: Object }}
     */
    // @ts-ignore
    const { props } = this;

    /**
     * @type {{ templateRepo: string, projectName: string, projectRepo: string, projectDescription: string, author: string }}
     */
    // @ts-ignore
    const { templateRepo, projectName, projectRepo, projectDescription, author } = props || {
      templateRepo: 'Base Template',
      projectName: '',
      projectRepo: 'git@gitlab.com:organization/repo.git',
      projectDescription: '',
      author: 'Generic Group'
    };

    /**
     * @type {{ repo: string }}
     */
    let { repo } = templates[templates.findIndex(item => item.name === templateRepo)];

    const gitObj = gitParser(projectRepo);
    const group = gitObj.owner.split('/')[0];
    const pathArr = [];
    for (const item of gitObj.owner.split('/')) {
      if (item === group) continue;
      pathArr.push(item);
    }

    const pageDoc = `https://${group}.gitlab.io/${pathArr.join('/')}/${gitObj.name}/`;

    if (process.env.CI_BUILD_TOKEN) {
      repo = repo.replace('git@gitlab.com:', `https://gitlab-ci-token:${process.env.CI_BUILD_TOKEN}@gitlab.com/`).replace('.git', '');
    }

    await git().clone(repo, targetDir, {
      template: '.'
    });

    const list = fs.readdirSync(targetDir);

    const promises = [];
    for (const item of list) {
      if (item !== '.git' && item !== '.gitmodules') {
        promises.push(fse.copy(`${targetDir}/${item}`, `./${item}`));
      }
    }

    await Promise.all(promises);

    // removes techdoc to allow cloning to work
    await fse.remove('hugo/themes/techdoc');

    await fse.remove(targetDir);

    try {
      const packageJs = JSON.parse(fs.readFileSync('./package.json', 'utf8'));
      let docs = fs.readFileSync('./docs/_index.md', 'utf8');
      const configToml = fs.readFileSync('./config.toml', 'utf8');

      docs = docs.replace(`title: "${templateRepo}"`, `title: "${projectName}"`);

      const docsConfig = toml.parse(configToml);

      docsConfig.baseURL = pageDoc;
      // @ts-ignore
      docsConfig.title = projectName;

      // @ts-ignore
      packageJs.name = slug(projectName, {
        lower: true
      });
      if (packageJs.repository && typeof packageJs.repository === 'object') {
        packageJs.uplbhcilabTemplate = {
          version: packageJs.version,
          templateName: templateRepo,
          templateRepo: packageJs.repository.url
        };

        packageJs.repository.url = projectRepo;
        packageJs.version = '0.0.1';
        packageJs.author = author;
      }
      packageJs.description = projectDescription;

      fs.writeFileSync('./docs/_index.md', docs, 'utf8');
      fs.writeFileSync('./config.toml', toml.stringify(docsConfig), 'utf8');
      fs.writeFileSync('./package.json', JSON.stringify(packageJs), 'utf8');
    } catch (error) {
      console.error(error);
    }

    let readme = '';

    try {
      readme = fs.readFileSync('./README.md', 'utf8');
    } catch (error) {
      console.log('No README found');
    }

    const readmeArr = readme.split('\n\n');

    // [![pipeline status](${}/badges/master/pipeline.svg)](https://gitlab.com/uplb-hci-lab/templates/base-template/-/commits/master)
    readmeArr[0] = `# ${projectName} ` +
    `[![pipeline status](https://${gitObj.source}/${gitObj.owner}/${gitObj.name}/badges/master/pipeline.svg)]` +
    `(https://${gitObj.source}/${gitObj.owner}/${gitObj.name}/-/commits/master)` +
    `[![coverage report](https://${gitObj.source}/${gitObj.owner}/${gitObj.name}/badges/master/coverage.svg)]` +
    `(https://${gitObj.source}/${gitObj.owner}/${gitObj.name}/-/commits/master)`;

    readmeArr[1] = projectDescription;

    readmeArr[2] = `Page Documentation can be found at: ${pageDoc}`;

    fs.writeFileSync('./README.md', readmeArr.join('\n\n'), 'utf8');
  }

  async install () {
    this.installDependencies({
      npm: true,
      bower: false
    });

    try {
      await git().submoduleAdd('https://github.com/thingsym/hugo-theme-techdoc.git', 'hugo/themes/techdoc');
    } catch (error) {
      this.log('Cannot pull from submodule, please run `npm run install-docs` later');
    }

    this.log(
      // @ts-ignore
      yosay('Now everything is set. Additional things to remember, you need hugo (https://gohugo.io/getting-started/installing) to serve docs locally')
    );
  }
};

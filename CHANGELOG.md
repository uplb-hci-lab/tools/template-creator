# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.7](https://gitlab.com/uplb-hci-lab/tools/template-creator/compare/v0.0.6...v0.0.7) (2020-03-30)


### Features

* add repo ([12f01d4](https://gitlab.com/uplb-hci-lab/tools/template-creator/commit/12f01d4fa0de7ce5e4ac6b3cce35645eee897baf))

### [0.0.6](https://gitlab.com/uplb-hci-lab/tools/template-creator/compare/v0.0.5...v0.0.6) (2020-03-25)


### Bug Fixes

* add a style fix on the creation of readme ([7741b4c](https://gitlab.com/uplb-hci-lab/tools/template-creator/commit/7741b4cdfb0071a9444cf8dca5f8656e4477d97d))

### [0.0.5](https://gitlab.com/uplb-hci-lab/tools/template-creator/compare/v0.0.4...v0.0.5) (2020-03-22)


### Features

* add ability to add page ([30eb4b8](https://gitlab.com/uplb-hci-lab/tools/template-creator/commit/30eb4b85ea968bc6b35c372e4a94676c6b171581))

### [0.0.4](https://gitlab.com/uplb-hci-lab/tools/template-creator/compare/v0.0.3...v0.0.4) (2020-03-18)

### [0.0.3](https://gitlab.com/uplb-hci-lab/tools/template-creator/compare/v0.0.2...v0.0.3) (2020-03-18)


### Bug Fixes

* **deps:** update dependency chalk to v3 ([b59bb88](https://gitlab.com/uplb-hci-lab/tools/template-creator/commit/b59bb8805c9b539c8e53ca8215a7e561db74e330))
* **deps:** update dependency yeoman-generator to v4 ([50755ae](https://gitlab.com/uplb-hci-lab/tools/template-creator/commit/50755ae09c11911fd040838847643dc0c6cd9655))

### [0.0.2](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/compare/v0.0.1...v0.0.2) (2020-03-04)


### Bug Fixes

* add fs-extra ([62c3893](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/62c3893b45ebf0f468fb7250f9ff5489997f9cc7))

### 0.0.1 (2020-03-04)

### 0.0.3 (2020-03-04)

### 0.0.2 (2020-01-18)


### Features

* add ability to create base template ([7b8e4c7](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/7b8e4c7f14f36d9744e95dd86a8382f7575c10f2))
* add docs and add ability docs ([bbfb597](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/bbfb5973c2a6da9e3826154ae6808cb1fa289a34))
* add docs and add ability to add docs ([5badf09](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/5badf09bc05c4717b8b999381e3e818029e3d2ae))
* add python 3.7 template ([e0b22ad](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/e0b22ad2038504d24566b7f7f1b9b53cccc85205))
* add restana js template ([8b910a2](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/8b910a21dd98c2e4944ab5b715ef192a5d5bfa0c))
* first commit ([be3d956](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/be3d9564e1948edb112c0fc7a1d8ad579712ec12))


### Bug Fixes

* use senti-techlabs as scope ([eb43b1e](https://gitlab.com/senti-techlabs/tools/senti-project-template-generator/commit/eb43b1e4082f6791d03ed4c9a839aea40b7516bf))

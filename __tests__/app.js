'use strict';
const path = require('path');
const assert = require('yeoman-assert');
const helpers = require('yeoman-test');

const testObj = {
  projectName: 'Test Project',
  projectDescription: 'Test Project Description',
  projectRepo: 'git@gitlab.com:senti-techlabs/templates/python-fastapi-backend-template.git',
  templateRepo: 'Base Template'
};

describe('generator-uplb-hci-lab-project-template:app', () => {
  beforeAll(async () => {
    return helpers
      .run(path.join(__dirname, '../generators/app'))
      .withPrompts(testObj);
  });

  it('creates all files from repository and it has the right content', async () => {
    assert.file([
      '.babelrc',
      '.eslintignore',
      '.gitlab',
      '.eslintrc.js',
      '.gitignore',
      '.gitlab-ci.yml',
      'CHANGELOG.md',
      'CODE_OF_CONDUCT.md',
      'commitlint.config.js',
      'CONTRIBUTING.md',
      'LICENSE',
      'package.json',
      'README.md',
      'renovate.json',
      'tsconfig-google.json',
      'tsconfig.json',
      'config.toml',
      'hugo',
      'docs'
    ]);

    assert.fileContent('README.md', new RegExp(`# ${testObj.projectName}`));
    assert.fileContent('README.md', new RegExp(`${testObj.projectDescription}`));

    assert.fileContent('config.toml', new RegExp('baseURL = "https://senti-techlabs.gitlab.io/templates/python-fastapi-backend-template/"'));

    assert.fileContent('docs/_index.md', new RegExp(`title: "${testObj.projectName}"`));
  });
});

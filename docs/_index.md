---
title: "UPLB HCI Lab Template Creator"
date: 2020-01-17
draft: false
---

This is the lab's template creator

This is based on Senti's project template creator

# Installation

First, install [Yeoman](http://yeoman.io) and generator-uplb-hci-lab-template using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g @tjmonsi/uplb-hci-lab-project-template
```

# Usage

Then generate your new project:

```bash
yo @tjmonsi/uplb-hci-lab-project-template
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

Apache-2.0 © [Toni-Jan Keith Monserrat]()
